<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">

		
		<div class="container top210">
			<div class="row">

				<!-- menu -->
				<div class="col-xs-12">
					<ul class="menu-rodape">
						<li><a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "a-unaflor"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/a-unaflor">CONHEÇA A UNAFLOR</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/promocoes">PROMOÇÕES</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
					</ul>
				</div>
				<!-- menu -->










				<!--  endereco, telefone -->
				<div class="col-xs-12 top20 telefone-rodape">
					<p class="pull-left"><i class="icon-telefone"></i><span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?></p>

					<?php if (!empty($config[telefone2])) { ?>
					<p class="left35 pull-left"><i class="icon-telefone"></i><span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?></p>
					<?php } ?>

					<?php if (!empty($config[telefone3])) { ?>
					<p class="left35 pull-left"><i class="fa fa-whatsapp" aria-hidden="true"></i><span>  <?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?></p>
					<?php } ?>

					<?php if (!empty($config[telefone4])) { ?>
					<p class="left35 pull-left"><i class="icon-telefone"></i><span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?></p>
					<?php } ?>
				</div>
				<!-- endereco, telefone -->



				<div class="clearfix"></div>




				<!-- ======================================================================= -->
				<!-- categorias	-->
				<!-- ======================================================================= -->
				<div class="col-xs-8 top35">
					<ul class="lista-categorias-rodape">
						<?php 
						$result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 7");
						if (mysql_num_rows($result) > 0) {
							while($row = mysql_fetch_array($result)){
								?>
								<li>
									<a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										<img height="25" width="25" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
										<br>
										<?php Util::imprime($row[titulo]); ?>
									</a>
								</li>  
								<?php 
							}
						}
						?>
					</ul>
				</div>


				<div class="col-xs-4 pg0">
					<img class="pull-left" src="<?php echo Util::caminho_projeto() ?>/imgs/bandeiras_pagseguro.jpg" alt="">
					<img class="pull-right" src="<?php echo Util::caminho_projeto() ?>/imgs/img-pagamento.jpg" alt="">
				</div>
				<!-- ======================================================================= -->
				<!-- categorias	-->
				<!-- ======================================================================= -->




				<!-- logo e botoes -->
				<div class="col-xs-8 top35">
					<a title="TEMPO E FORMAS DE ENTREGAS" class="right20" href="javascript:void(0);" data-toggle="modal" data-target="#myModal-rodape">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-formas-entrega.png" alt="TEMPO E FORMAS DE ENTREGAS">
					</a>


					<!-- Modal -->
		            <div class="modal fade modal-rodape" id="myModal-rodape" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		              <div class="modal-dialog" role="document">
		                <div class="modal-content">
		                  <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                    <h2 class="modal-title" id="myModalLabel">TEMPO E FORMAS DE ENTREGA</h2>
		                  </div>
		                  <div class="modal-body">
		                    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
		                    <p><?php Util::imprime($row[descricao]); ?></p>
		                  </div>
		                  <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		                  </div>
		                </div>
		              </div>
		            </div>
		            <!-- /.modal -->



					<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal1-rodape" title="FORMAS DE PAGAMENTOS">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-formas-pagamentos.png" alt="FORMAS DE PAGAMENTOS">
					</a>

					<!-- Modal -->
		              <div class="modal fade modal-rodape" id="myModal1-rodape" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		                <div class="modal-dialog" role="document">
		                  <div class="modal-content">
		                    <div class="modal-header">
		                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                      <h2 class="modal-title" id="myModalLabel">FORMAS DE PAGAMENTO</h2>
		                    </div>
		                    <div class="modal-body">
		                      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
		                      <p><?php Util::imprime($row[descricao]); ?></p>
		                    </div>
		                    <div class="modal-footer">
		                      <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		                    </div>
		                  </div>
		                </div>
		              </div>
		              <!-- /.modal -->




				</div>


				<div class="col-xs-4 text-right top25">
					<?php if ($config[google_plus] != "") { ?>
					<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-google-plus right15" style="color: #000"></i>
					</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-homeweb.png"  alt="">
					</a>
				</div>	
				<!-- logo -->



				<div class="col-xs-12 text-center top30 direitos">
					<h5>© Copyright UNAFLOR</h5>
				</div>
				
			</div>
		</div>
	</div>
</div>





