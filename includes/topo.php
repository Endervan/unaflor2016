
<div class="container">
	<div class="row">
		<!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->
		<div class="col-xs-2 posicao-logo top10 text-center">
			<a href="<?php echo Util::caminho_projeto() ?>/" title="">
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
			</a>
		</div>
		<!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->

		<!--  ==============================================================  -->


		<div class="col-xs-10 padding0">
			<!--  ==============================================================  -->
			<!-- CONTATOS -->
			<!--  ==============================================================  -->
			<div class="col-xs-7 contatos padding0">





				<?php if (!empty($config[telefone3])): ?>
				<div class="media pull-right">
					<div class="media-left media-middle">
						<i class="fa fa-whatsapp" aria-hidden="true"></i><span>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><span><?php Util::imprime($config[ddd3]); ?></span> <?php Util::imprime($config[telefone3]); ?></h3>
					</div>
				</div>
				<?php endif; ?>


				<?php if (!empty($config[telefone2])): ?>
				<div class="media pull-right right10">
					<div class="media-left media-middle">
						<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone.png" alt="">
					</div>
					<div class="media-body">
						<h3 class="media-heading"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h3>
					</div>
				</div>
				<?php endif; ?>



				<div class="media pull-right right10">
					<div class="media-right media-middle">
						<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone.png" alt="">
					</div>
					<div class="media-body">
						<h3 class="media-heading"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h3>
					</div>
				</div>




			</div>
			<!--  ==============================================================  -->
			<!-- CONTATOS -->
			<!--  ==============================================================  -->


			<!--  ==============================================================  -->
			<!-- BARRA PESQUISA -->
			<!--  ==============================================================  -->
			<div class="col-xs-5 pesquisa">
				<div class="col-xs-10 ">
					<form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
						<div class="input-group stylish-input-group">
							<input type="text" class="form-control" name="busca_produtos" placeholder="O QUE ESTÁ PROCURANDO?" >
							<span class="input-group-addon">
								<button type="submit">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</form>
				</div>




					<div class="dropdown col-xs-2">
					  <a class="usuario-topo" id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					    <i class="fa fa-bars" aria-hidden="true"></i>
					  </a>

					  <ul class="dropdown-menu pull-right lista-categorias-submenu menu-topo-usuario" aria-labelledby="dLabel-topo">
					  	<?php if (!isset($_SESSION[usuario])): ?>
					    		<li><a href="<?php echo Util::caminho_projeto() ?>/autenticacao"><i class="fa fa-sign-in" aria-hidden="true"></i> Entrar</a></li>
					     <?php else: ?>
					     	<li><a href="<?php echo Util::caminho_projeto() ?>/meus-pedidos"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Meus pedidos</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/meus-dados"><i class="fa fa-user" aria-hidden="true"></i>  Meus dados</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/alterar-senha"><i class="fa fa-key" aria-hidden="true"></i> Alterar senha</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/logoff"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
					     <?php endif ?>
					  </ul>
					</div>





			</div>
			<!--  ==============================================================  -->
			<!-- BARRA PESQUISA -->
			<!--  ==============================================================  -->



			<!--  ==============================================================  -->
			<!-- MENU-->
			<!--  ==============================================================  -->
			<div class="col-xs-12">
				<div class="menu_topo">
					<ul class="top15">
						<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/">INÍCIO</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "a-unaflor"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/a-unaflor">CONHEÇA A UNAFLOR</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/promocoes">PROMOÇÕES</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
						</li>

						<li>
							<a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
						</li>
					</ul>
				</div>
			</div>
			<!--  ==============================================================  -->
			<!-- MENU-->
			<!--  ==============================================================  -->

		</div>


	</div>
</div>




<!--  ==============================================================  -->
<!--SEGUNDO MENU TOPO-->
<!--  ==============================================================  -->
<div class="container bg_menu pr0">
	<div class="row">

		<!--  ==============================================================  -->
		<!-- MENU-->
		<!--  ==============================================================  -->
		<div class="col-xs- col-xs-offset-2 lista-categorias text-center">
			<!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
			<div id="navbar">
				<ul class="nav navbar-nav">
					<li>
						<a href="<?php echo Util::caminho_projeto() ?>/produtos">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_menu01.png" alt="">
							<br>
							VER TODAS
						</a>
					</li>


					<?php
					$result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 0, 5");
					if (mysql_num_rows($result) > 0) {
						while($row = mysql_fetch_array($result)){
						?>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<img height="27" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
								<span class="clearfix"><?php Util::imprime($row[titulo]); ?></span>
							</a>
						</li>
						<?php
						}
					}
					?>




					<!--  ==============================================================  -->
					<!-- VER MAIS-->
					<!--  ==============================================================  -->
					<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
						<a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_menu01.png" alt="">
							<br>
							VER MAIS
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu lista-categorias-submenu pull-right pg10 " aria-labelledby="drop3">


							<?php
							$result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 5, 30");
							if (mysql_num_rows($result) > 0) {
								while($row = mysql_fetch_array($result)){
								?>
								<li class="top10">
									<a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										<img height="25" width="25" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
										<?php Util::imprime($row[titulo]); ?>
									</a>
								</li>
								<?php
								}
							}
							?>


						</ul>
					</li>
					<!--  ==============================================================  -->
					<!-- VER MAIS-->
					<!--  ==============================================================  -->
				</ul>

			</div><!--/.nav-collapse -->
		</div>
		<!--  ==============================================================  -->
		<!-- MENU-->
		<!--  ==============================================================  -->

		<!--  ==============================================================  -->
		<!--CARRINHO-->
		<!--  ==============================================================  -->
		<div class="col-xs-3 dropdown pr0">
			<button class="btn btn_carrinho input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]); ?></span>
			</button>


			<div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">


				<?php
				if(count($_SESSION[solicitacoes_produtos]) > 0)
				{
					for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
					{
						$row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
						?>
						<div class="lista-itens-carrinho">
							<div class="col-xs-2">
								<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
							</div>
							<div class="col-xs-8">
								<h1><?php Util::imprime($row[titulo]) ?></h1>
							</div>
							<div class="col-xs-1">
								<a href="<?php echo Util::caminho_projeto() ?>/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
							</div>
						</div>
						<?php
					}
				}
				?>



				<div class="text-right bottom20">
					<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-vermelho" >
						ENVIAR ORÇAMENTO
					</a>
				</div>
			</div>
		</div>
		<!--  ==============================================================  -->
		<!--CARRINHO-->
		<!--  ==============================================================  -->


	</div>
</div>

<!--  ==============================================================  -->
<!--SEGUNDO MENU TOPO-->
<!--  ==============================================================  -->
