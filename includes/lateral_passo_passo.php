 <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->
          <div class="contatos">
            <div class="media top45">
              <div class="media-left media-middle">       
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone.png" alt="">
              </div>
              <div class="media-body">
                <h3 class="media-heading">ATENDIMENTO</h3>
              </div>
            </div>
            <div class="top30">
              <h3 class="media-heading">
                  <span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>
                  
                  <?php if (!empty($config[telefone2])): ?>
                    <span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?>  
                  <?php endif ?>

                  <?php if (!empty($config[telefone3])): ?>
                    <span><?php Util::imprime($config[ddd3]); ?></span> <?php Util::imprime($config[telefone3]); ?>  
                  <?php endif ?>

                  <?php if (!empty($config[telefone4])): ?>
                    <span><?php Util::imprime($config[ddd4]); ?></span> <?php Util::imprime($config[telefone4]); ?>  
                  <?php endif ?>

              </h3>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->


          <!--  ==============================================================  -->
          <!-- MENU -->
          <!--  ==============================================================  -->
          <ul class="nav nav-pills nav-stacked carrinho_formas top35">
            <li role="presentation" class="<?php if($passo == 1){ echo "active"; } ?> "><span>1</span>MEU CARRINHO<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
            <li role="presentation" class="<?php if($passo == 2){ echo "active"; } ?> "><span>2</span>IDENTIFICAÇÃO<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
            <li role="presentation" class="<?php if($passo == 3){ echo "active"; } ?> local"><span>3</span>LOCAL DE ENTREGA MENSAGEM<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
            <li role="presentation" class="<?php if($passo == 4){ echo "active"; } ?> "><span>4</span>PAGAMENTO<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
            <li role="presentation" class="<?php if($passo == 5){ echo "active"; } ?> "><span>5</span>MEUS PEDIDOS<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
            <li role="presentation" class="<?php if($passo == 6){ echo "active"; } ?> "><span> <i class="fa fa-user" aria-hidden="true"></i> &nbsp; </span>MEUS DADOS<i class="fa fa-check-square-o" aria-hidden="true"></i></li>
          </ul>
          <!--  ==============================================================  -->
          <!-- MENU -->
          <!--  ==============================================================  -->
