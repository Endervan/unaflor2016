<?php
session_start();
require_once("../../class/Include.class.php");
require_once("../trava.php");
require_once("Seo_Model.php");


$obj_control = new Seo_Model();



//  DESATIVA OU ATIVA UM ITEM
if(isset($_GET[action]) and $_GET[action] == "ativar_desativar")
{
  $obj_control->ativar_desativar($_GET[id], $_GET[ativo]);
  //Util::script_msg("Registro alterado com sucesso");
  Util::script_location("index.php");
}


//  EXCLUIR
if(isset($_GET[action]) and $_GET[action] == "excluir")
{
  $obj_control->excluir($_GET[id]);
  //Util::script_msg("Registro excluído com sucesso.");
  Util::script_location("index.php");
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("../includes/head.php") ?>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body>


	<!-- ======================================================================= -->
	<!-- topo	-->
	<!-- ======================================================================= -->
	<?php require_once("../includes/topo.php"); ?>





	<div class="container-fluid">
      <div class="row">


      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-2 sidebar">
      	<?php require_once("../includes/lateral.php"); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->





      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-10 main ">
            <?php
                  $result = $obj_control->select();
                  
                  
                  
                  if(mysql_num_rows($result) == 0)
                  {
                        echo "<h1>Nenhum resultado cadastrado</h1>";    
                  }
                  else
                  {
                  ?>
            
            <form action="" method="post" name="form_listagem" id="form_listagem" enctype="multipart/form-data">
                <div class="tabela-holder">

                    <!-- ======================================================================= -->
                <!-- PESQUISA  -->
                <!-- ======================================================================= -->
                <?php require_once("../includes/pesquisa_tabelas.php"); ?>
                <!-- ======================================================================= -->
                <!-- PESQUISA  -->
                <!-- ======================================================================= -->  
                
                    <table class="table table-hover"> 
                        <thead> 
                            <tr>
                                <th width="34">COD</th>
                                <th width="469">PÁGINA</th>
                                <th width="41">EDITAR</th>
                            </tr> 
                        </thead> 
                        <tbody class="searchable"> 
                           <?php
                           while($row = mysql_fetch_array($result))
                           {
                           ?>
                            <tr>
                              <td class="titulo-componente-tabela"><?php Util::imprime($row[0]); ?></td>
                              <td class="titulo-componente-tabela"> 
                                                  <?php Util::imprime($row[titulo]); ?>
                                </td>
                                
                                
                            
                                
                                <td align="center">
                                    <a href="altera.php?action=alterar&id=<?php Util::imprime($row[0]); ?>" data-toggle="tooltip" data-placement="top" title="Editar">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                </td> 
                               
                            </tr>
                            <?php
                           }
                            ?>
                        </tbody> 
                    </table> 
        </div>
                
                <div class="tabela-fundo">
                    &nbsp;
                </div>
            
            </form>
            <?php
                  }
                  ?>    
      </div>
      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->


        




</body>
</html>