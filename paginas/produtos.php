<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
  .bg-interna {
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->






  <div class="container">
    <div class="row produtos_destaques">
      <div class="col-xs-6 top60">
        <blockquote>
          <h2>CONFIRA NOSSOS<br><span>PRODUTOS</span></h2>
        </blockquote>
        <div class="top90 text-right">

        </div>
      </div>


      <!--  ==============================================================  -->
      <!--PRODUTOS DESTAQUES-->
      <!--  ==============================================================  -->
      <div class="col-xs-6 produtos_home padding0">


        <?php
        $result = $obj_site->select("tb_produtos", "and id_categoriaproduto IN(74, 75) order by rand() limit 2");
        if (mysql_num_rows($result) > 0) {
          while ($row = mysql_fetch_array($result)) {
        ?>
            <!--  ==============================================================  -->
            <!-- ITEM 01-->
            <!--  ==============================================================  -->
            <div class="col-xs-6 top5">

              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 259, array("class" => "", "alt" => "$row[titulo]")) ?>
                <h1><?php Util::imprime($row[titulo]); ?></h1>
                <?php if ($config['pausar_vendas'] != "SIM") : ?>
                  <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>
                <?php endif; ?>
              </a>


              <?php if ($config['pausar_vendas'] == "SIM") : ?>
                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="ESGOTADO" class="btn-produto-esgotado top10">
                  PRODUTO ESGOTADO
                </a>
              <?php else : ?>

                <?php
                // VERIFICA SE O PRODUTO TEM PREÇO
                if (!empty($row[preco])) {
                  // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
                ?>
                  <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                    <button type="submit" class="btn btn_produtos_home top10">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                      COMPRAR AGORA
                    </button>



                    <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                    <input type="hidden" name="action" value="add" />
                  </form>
                <?php } else { ?>
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10">
                    SAIBA MAIS
                  </a>
                <?php } ?>

              <?php endif; ?>


            </div>
        <?php
          }
        }
        ?>



      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS DESTAQUES-->
  <!--  ==============================================================  -->




  <div class="container top50">
    <div class="row">
      <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

        <!--  ==============================================================  -->
        <!-- CATEGORIAS-->
        <!--  ==============================================================  -->
        <div class="col-xs-3 categorias_home">
          <div class="list-group">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item active" title="Todas as Categorias">TODAS AS CATEGORIAS</a>
            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>


                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item" title="<?php Util::imprime($row[titulo]); ?>">
                  <blockquote><?php Util::imprime($row[titulo]); ?></blockquote>
                </a>
            <?php
              }
            }
            ?>

          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- CATEGORIAS-->
        <!--  ==============================================================  -->


        <div class="col-xs-9 padding0">
          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR-->
          <!--  ==============================================================  -->
          <div class="col-xs-6">
            <select name="select_categorias" class="selectpicker submit-select" data-live-search="true" title="ORDENAR POR" data-width="100%">
              <optgroup label="SELECIONE">
                <?php
                //VERIFICO SE FIO SELECIONADO A CATEGORIA
                if (Url::getURL(1) == 'ordem' or Url::getURL(1) == '') {
                  $url_ordem = 'ordem';
                } else {
                  $url_ordem = Url::getURL(1);
                }
                ?>
                <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/menor-preco" data-tokens="MENOR PREÇO">MENOR PREÇO</option>
                <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/maior-preco" data-tokens="MAIOR PREÇO">MAIOR PREÇO</option>
                <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/mais-vendidos" data-tokens="MAIS VENDIDOS">MAIS VENDIDOS</option>
                <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/novos-produtos" data-tokens="NOVOS PRODUTOS">NOVOS PRODUTOS</option>

              </optgroup>
            </select>
          </div>
          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR-->
          <!--  ==============================================================  -->




          <div class="col-xs-6">

            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <div class="input-group stylish-input-group">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

                <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA">
                <span class="input-group-addon">
                  <button type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>

              </form>

            </div>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>

          <div class="col-xs-12 top15">
            <div class="borda_rosa"></div>
          </div>


          <!--  ==============================================================  -->
          <!--PRODUTOS HOME-->
          <!--  ==============================================================  -->

          <div class="produtos_home">



            <?php
            //  FILTRA PELO TITULO
            if (isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos])) :
              $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
            endif;



            // ORDEM DOS PRODUTOS
            switch (Url::getURL(2)) {
              case 'menor-preco':
                $complemento_ordem .= "order by preco";
                break;
              case 'maior-preco':
                $complemento_ordem .= "order by preco desc";
                break;
              case 'mais-vendidos':
                $complemento_ordem .= "order by qtd_vendida desc";
                break;
              case 'novos-produtos':
                $complemento_ordem .= "order by idproduto desc";
                break;
            }







            if (Url::getURL(1) != 'ordem' and Url::getURL(1) != '') {

              $url1 = Url::getURL(1);
              //  FILTRA AS CATEGORIAS
              if (isset($url1)) {
                $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);

                $sql = "
                    SELECT
                    tp.*, tpc.id_categoriaproduto
                    FROM
                    tb_produtos tp, tb_produtos_categorias tpc
                    WHERE
                    tp.idproduto = tpc.id_produto
                    AND tpc.id_categoriaproduto IN($id_categoria)
                    AND tp.ativo = 'SIM'
                    GROUP BY
                    tp.idproduto
                    $complemento_ordem
                    ";
              }
            } else {
              $sql = "SELECT * FROM tb_produtos where ativo = 'SIM' $complemento $complemento_ordem";
            }





            $result = $obj_site->executaSQL($sql);
            if (mysql_num_rows($result) == 0) {
              echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
            } else {
              $i = 0;
              while ($row = mysql_fetch_array($result)) {
            ?>
                <div class="col-xs-4 top20">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 259, array("class" => "", "alt" => "$row[titulo]")) ?>
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <h5><?php Util::imprime($row[codigo]); ?></h5>
                    <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>
                  </a>

                  <?php if ($config['pausar_vendas'] == "SIM") : ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="ESGOTADO" class="btn-produto-esgotado top10">
                      PRODUTO ESGOTADO
                    </a>
                  <?php else : ?>


                    <?php
                    // VERIFICA SE O PRODUTO TEM PREÇO
                    if (!empty($row[preco])) {
                      // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
                    ?>
                      <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                        <button type="submit" class="btn btn_produtos_home top10">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                          COMPRAR AGORA
                        </button>



                        <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                        <input type="hidden" name="action" value="add" />
                      </form>
                    <?php } else { ?>
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10">
                        SAIBA MAIS
                      </a>
                    <?php } ?>

                  <?php endif; ?>

                </div>
            <?php
                if ($i == 2) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                } else {
                  $i++;
                }
              }
            }
            ?>

          </div>


        </div>

      </form>



    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->








  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>