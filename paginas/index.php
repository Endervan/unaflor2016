<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<body>

  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                  ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" />
                  <?php
                  } else {
                  ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" />
                    </a>
                  <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
            <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->



  <div class="pagina">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->

  </div>

  <!--  ==============================================================  -->
  <!--BOTOES FORMAS E ENTREGAS -->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row subir_forma_entregas">

      <div class="col-xs-2 bottoes_entrega text-right">
        <a class="btn btn_transparente_entrega" href="javascript:void(0);" title="TEMPO E FORMA ENTREGAS" data-toggle="modal" data-target="#myModal-rodape">
          TEMPO E FORMA<span class="clearfix">ENTREGAS</span>
          <span class="posicao_entrega01"></span>
        </a>
      </div>

      <div class="col-xs-2 bottoes_entrega text-right padding0">
        <a class="btn btn_transparente_entrega" href="javascript:void(0);" data-toggle="modal" data-target="#myModal1-rodape" title="FORMAS DE PAGAMENTOS">
          FORMAS DE<span class="clearfix">PAGAMENTOS</span>
          <span class="posicao_entrega"></span>
        </a>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--BOTOES FORMAS E ENTREGAS -->
  <!--  ==============================================================  -->


  <div class="container top50">
    <div class="row">
      <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

        <!--  ==============================================================  -->
        <!-- CATEGORIAS-->
        <!--  ==============================================================  -->
        <div class="col-xs-3 categorias_home">
          <div class="list-group">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item active" title="Todas as Categorias">TODAS AS CATEGORIAS</a>
            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>


                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item" title="<?php Util::imprime($row[titulo]); ?>">
                  <blockquote><?php Util::imprime($row[titulo]); ?></blockquote>
                </a>
            <?php
              }
            }
            ?>

          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- CATEGORIAS-->
        <!--  ==============================================================  -->


        <div class="col-xs-9 padding0">

          <?php /*
                <!--  ==============================================================  -->
                <!-- BARRA CATEGORIA ORDENA POR-->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <select name="select_categorias" class="selectpicker submit-select" data-live-search="true" title="ORDENAR POR" data-width="100%">
                    <optgroup label="SELECIONE">
                      <?php
                      //VERIFICO SE FIO SELECIONADO A CATEGORIA
                      if(Url::getURL(1) == 'ordem' or Url::getURL(1) == '' ){
                        $url_ordem = 'ordem';
                      }else{
                        $url_ordem = Url::getURL(1);
                      }
                      ?>
                      <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/menor-preco" data-tokens="MENOR PREÇO">MENOR PREÇO</option>
                      <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/maior-preco" data-tokens="MAIOR PREÇO">MAIOR PREÇO</option>
                      <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/mais-vendidos" data-tokens="MAIS VENDIDOS">MAIS VENDIDOS</option>
                      <option value="<?php echo Util::caminho_projeto() ?>/produtos/<?php echo $url_ordem; ?>/novos-produtos" data-tokens="NOVOS PRODUTOS">NOVOS PRODUTOS</option>

                    </optgroup>
                  </select>
                </div>
                <!--  ==============================================================  -->
                <!-- BARRA CATEGORIA ORDENA POR-->
                <!--  ==============================================================  -->




                <div class="col-xs-offset-6 col-xs-6">

                  <!--  ==============================================================  -->
                  <!--BARRA PESQUISAS-->
                  <!--  ==============================================================  -->
                  <div class="input-group stylish-input-group">
                    <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

                      <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >
                      <span class="input-group-addon">
                        <button type="submit">
                          <span class="glyphicon glyphicon-search"></span>
                        </button>
                      </span>

                    </form>

                  </div>
                  <!--  ==============================================================  -->
                  <!--BARRA PESQUISAS-->
                  <!--  ==============================================================  -->
                </div>

*/ ?>
          <div class="col-xs-12 produtos_destaques_home">
            <h1>PRODUTOS EM DESTAQUES</h1>
          </div>


          <div class="col-xs-12 top15">
            <div class="borda_rosa"></div>
          </div>


          <!--  ==============================================================  -->
          <!--PRODUTOS HOME-->
          <!--  ==============================================================  -->

          <div class="produtos_home">









            <?php
            $sql = "
                              SELECT
                                tp.*, tpc.id_categoriaproduto
                              FROM
                                tb_produtos tp, tb_produtos_categorias tpc
                              where 
                                tp.idproduto = tpc.id_produto
                                AND tp.ativo = 'SIM'
                                AND tpc.id_categoriaproduto IN(74, 75)
                                order by rand() 
                                limit 6
                             ";

            $result = $obj_site->executaSQL($sql);
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>


                <div class="col-xs-4 top20">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 259, array("class" => "", "alt" => "$row[titulo]")) ?>
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <h5><?php Util::imprime($row[codigo]); ?></h5>

                    <?php if ($config['pausar_vendas'] != "SIM") : ?>
                      <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>
                    <?php endif; ?>
                  </a>


                  <?php if ($config['pausar_vendas'] == "SIM") : ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="ESGOTADO" class="btn-produto-esgotado top10">
                      PRODUTO ESGOTADO
                    </a>
                  <?php else : ?>

                    <?php
                    // VERIFICA SE O PRODUTO TEM PREÇO
                    if (!empty($row[preco])) {
                      // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
                    ?>
                      <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">
                        <button type="submit" class="btn btn_produtos_home top10">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                          COMPRAR AGORA
                        </button>
                        <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                        <input type="hidden" name="action" value="add" />
                      </form>
                    <?php } else { ?>
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10">
                        SAIBA MAIS
                      </a>
                    <?php } ?>

                  <?php endif; ?>

                </div>
            <?php
              }
            }
            ?>

          </div>


        </div>

      </form>



    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--NOSSAS MENSAGENS-->
  <!--  ==============================================================  -->
  <div class="container bottom50">
    <div class="row top40">
      <div class="col-xs-9 relative">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators carroucel-home">
            <?php
            $result = $obj_site->select("tb_promocoes", "order by rand() limit 3");
            if (mysql_num_rows($result) > 0) {
              $i = 0;
              while ($row = mysql_fetch_array($result)) {
                $imagens[] = $row;
            ?>

                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) {
                                                                                                        echo "active";
                                                                                                      } ?>"></li>
            <?php
                $i++;
              }
            }
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <?php
            if (count($imagens) > 0) {
              $i = 0;
              foreach ($imagens as $key => $imagem) {
            ?>
                <div class="item <?php if ($i == 0) {
                                    echo "active";
                                  } ?>">
                  <img class="<?php echo $i ?>-slide" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                  <div class="carousel-caption"></div>
                </div>

            <?php
                $i++;
              }
            }
            ?>
          </div>
          <div class="barra_transparente_mensagens effect2"></div>
        </div>

      </div>

      <!--  ==============================================================  -->
      <!-- SEGETÕES DE MENSAGENS-->
      <!--  ==============================================================  -->
      <div class="col-xs-3 sugestoes_home">
        <h3>SUGESTÕES DE <span>MENSAGENS</span></h3>


        <?php
        $result = $obj_site->select("tb_categorias_mensagens", "order by rand() limit 5");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while ($row = mysql_fetch_array($result)) {
        ?>
            <blockquote class="top10">
              <a href="<?php echo Util::caminho_projeto() ?>/sugestoes-mensagens/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <p><?php Util::imprime($row[titulo]); ?></p>
              </a>
            </blockquote>
        <?php
          }
        }
        ?>
      </div>
      <!--  ==============================================================  -->
      <!-- SEGETÕES DE MENSAGENS-->
      <!--  ==============================================================  -->
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--NOSSAS MENSAGENS-->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_conheca_mais">
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-xs-offset-5 col-xs-7">
            <blockquote class="top10">
              <h2>CONHEÇA UM POUCO MAIS A <br><span>UNAFLOR</span></h2>
            </blockquote>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <div class="top30">
              <p><?php Util::imprime($row[descricao], 500); ?></p>
            </div>
            <a class="btn btn_transparente_saiba pull-right top50" href="<?php echo Util::caminho_projeto() ?>/a-unaflor/" title="<?php Util::imprime($row[url_amigavel]); ?>">
              SAIBA MAIS
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<!-- slider JS files -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
<script>
  jQuery(document).ready(function($) {
    // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
    // it's recommended to disable them when using autoHeight module
    $('#content-slider-1').royalSlider({
      autoHeight: true,
      arrowsNav: false,
      fadeinLoadedSlide: false,
      controlNavigationSpacing: 0,
      controlNavigation: 'tabs',
      imageScaleMode: 'none',
      imageAlignCenter: false,
      loop: false,
      loopRewind: true,
      numImagesToPreload: 6,
      keyboardNavEnabled: true,
      usePreloader: false,
      autoPlay: {
        // autoplay options go gere
        enabled: true,
        pauseOnHover: true
      }

    });
  });
</script>