<?php 

$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto()."/produtos";
  header("location: $caminho");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto()."/produtos";
  header("location: $caminho");
endif;





// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 12);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 23) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">
      <div class="col-xs-12 top35">
       <blockquote>
        <h2>ENVIE SEU PEDIDO<br><span>MEU CARRINHO DE COMPRAS</span></h2>
      </blockquote>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--PROMOCOES DESTAQUES-->
<!--  ==============================================================  -->



<div class="container-fluid bg_fundo_internas">
  <div class="row">
    <div class="container">
      <div class="row">


        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4">
          <?php 
          $passo = 4;
          require_once("./includes/lateral_passo_passo.php"); 
          ?>
        </div>
        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->








        <div class="col-xs-8 top120">
        
            <?php
            if(isset($_POST[tipo_pagamento])):
              
              # ==============================================================  #
              # VERIFICO SE FOI ENVIADO A OS DADOS
              # ==============================================================  #
              if(isset($_POST[btn_cadastrar])):
                
                echo $obj_carrinho->finaliza_venda($_POST[tipo_pagamento]);
                
              endif;
            
            else:
            ?>
                          <form name="form_pagamento" id="form_pagamento" class="form_mensagem formulario1" action="" method="post" >
                              <h2>Selecione o tipo de pagamento desejado.</h2>
                              
                              
                              <ul>
                                  
                                  <li class="top20 bottom20">
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="" value="paypal" class="validate[required]" >
                                          PayPal
                                      </p>
                                  </li>
                                  <li class="top20 bottom20">
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="" value="pagseguro" class="validate[required]" >
                                          PagSeguro
                                      </p>
                                  </li>
                                  <li>
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="" value="deposito" class="validate[required]" >
                                          Depósito bancário
                                      </p>
                                  </li>                    
                            </ul> 
                            
                            <input type="submit" name="btn_cadastrar" id="btn_cadastrar" class="btn top30 btn-vermelho" value="ENVIAR" />
              </form>
                        
                          
            <?php
            endif;
            ?>
                 

        </div>



      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      cep: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      dataHora: {
        validators: {
          notEmpty: {

          }
        }
      },
      para: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

