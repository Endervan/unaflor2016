<?php
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto(). "/produtos";
  header("location: $caminho ");
endif;





# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if(isset($_POST[btn_cadastrar])):
  $obj_carrinho->armazena_mensagem_endereco_entrega($_POST);

  $caminho = Util::caminho_projeto(). "/pagamento";
  header("location: $caminho ");

endif;





// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- busca os horarios de entrega -->
  <script type="text/javascript">
  $(function() {
    $('#data_entrega').find(function(){
      $('#horario_marcado').load('<?php echo Util::caminho_projeto(); ?>/includes/horarios_entrega.php?id='+$('#data_entrega').val());
    });
  });
  </script>

  <!--exibindo select dos horarios_entrega -->
  <script type="text/javascript">
  $(document).ready(function(){
    $('#data_entrega').click(function(){
      $('.data_div').css("visibility","visible");
    });

  })
  </script>
  <!--exibindo select dos horarios_entrega -->


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">
      <div class="col-xs-12 top35">
        <blockquote>
          <h2>ENVIE SEU PEDIDO<br><span>MEU CARRINHO DE COMPRAS</span></h2>
        </blockquote>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PROMOCOES DESTAQUES-->
  <!--  ==============================================================  -->



  <div class="container-fluid bg_fundo_internas">
    <div class="row">
      <div class="container">
        <div class="row">



          <!-- ======================================================================= -->
          <!-- LATERAL ESQUERDA  -->
          <!-- ======================================================================= -->
          <div class="col-xs-4">
            <?php
            $passo = 3;
            require_once("./includes/lateral_passo_passo.php");
            ?>
          </div>
          <!-- ======================================================================= -->
          <!-- LATERAL ESQUERDA  -->
          <!-- ======================================================================= -->






          <div class="col-xs-8 top120 padding0">
            <div class="produtos_destaques">
              <div class="col-xs-12 padding0">
                <blockquote>
                  <h2><b>LOCAL DA ENTREGA</b></h2>
                </blockquote>
              </div>
            </div>

            <!--  ==============================================================  -->
            <!-- LOCAL DA ENTREGA-->
            <!--  ==============================================================  -->
            <div class="col-xs-12  fundo-formulario">
              <form class="form-inline FormMensagem pt30" role="form" method="post" enctype="multipart/form-data">


                <div class="col-xs-10 top10">
                  <div class="form-group  input100">
                    <p>BAIRRO DA ENTREGA: <b><?php Util::imprime( Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo") ) ?></b></p>
                  </div>
                </div>


                <div class="col-xs-2">
                  <a href="<?php echo Util::caminho_projeto() ?>/carrinho" class="btn btn-rosa-default" title="Alterar bairro">Alterar bairro</a>
                </div>


                <div class="clearfix"></div>

                <div class="col-xs-6 top10">
                  <div class="form-group  input100">
                    <input type="text" name="nome_contato" class="form-control fundo-form1 input-lg input100" placeholder="NOME DESTINATÁRIO">
                  </div>
                </div>


                <div class="col-xs-3 top10">
                  <div class="form-group  input100">
                    <input type="text" name="telefone_contato" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                  </div>
                </div>


                <div class="col-xs-3 top10">
                  <div class="form-group  input100">
                    <input type="text" name="celular_contato" class="form-control fundo-form1 input-lg input100" placeholder="CELULAR ">
                  </div>
                </div>





                <div class="clearfix"></div>


                <div class="col-xs-3 top10">
                  <div class="form-group  input100">
                    <input type="text" maxlength="8" name="cep_entrega" class="form-control fundo-form1 input-lg input100" placeholder="CEP">
                  </div>
                </div>

                <div class="col-xs-5 top10">
                  <div class="form-group  input100">
                    <input type="text" name="endereco_entrega" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">
                  </div>
                </div>

                <div class="col-xs-1 top10 pg0">
                  <div class="form-group  input100 pg0">
                    <input type="text" name="numero_entrega" class="form-control fundo-form1 input-lg input100 pg0 pl5" placeholder="NR">
                  </div>
                </div>

                <div class="col-xs-3 top10">
                  <div class="form-group  input100">
                    <input type="text" name="complemento_entrega" class="form-control fundo-form1 input-lg input100" placeholder="COMPLEMENTO">
                  </div>
                </div>




                <div class="clearfix"></div>


                <div class="col-xs-12 top10">
                  <div class="form-group  input100">
                    <input type="text" name="ponto_referencia" class="form-control fundo-form1 input-lg input100" placeholder="PONTO DE REFERÊNCIA">
                  </div>
                </div>


                <div class="clearfix"></div>

                <div class="col-xs-4">
                  <div class="form-group">
                    <div class="input-group date">
                      <input type="text" class="form-control fundo-form1 input-lg input100" id="data_entrega" name="data_entrega" placeholder="DATA ENTREGA"/>
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>


                <div class="col-xs-5 padding0 top15">
                  <div class="form-group  data_div input100">
                    <?php $obj_carrinho->select_horario_marcado("horario_entrega", "form-control fundo-form1 input-lg input100") ?>
                  </div>
                </div>





                <div class="col-xs-3 top15">
                  <div class="form-group  input100 horario_marcado_div">
                    <select class="form-control fundo-form1 input-lg input100" name="horario_marcado" id="horario_marcado" >
                      <option value="">HORÁRIO DESEJADO</option>
                    </select>
                  </div>
                </div>



                <div class="clearfix"></div>

                <div class="produtos_destaques top60">
                  <div class="col-xs-12 padding0">
                    <blockquote>
                      <h2>
                        <b>SUA MENSAGEM</b> <br>

                        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal1">
                          <span>CONFIRA ALGUMAS SUGESTÕES DE MENSAGENS</span>
                        </a>

                      </h2>
                    </blockquote>


                    <!-- Modal -->
                    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myModalLabel">SUGESTÕES DE MENSAGENS</h2>
                          </div>
                          <div class="modal-body">
                            <?php
                            $result = $obj_site->select("tb_sugestoes_mensagens", "order by rand() limit 5");
                            if(mysql_num_rows($result) == 0){
                              echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhuma mensagem encontrada.</p>";
                            }else{
                              $i = 0;
                              while($row = mysql_fetch_array($result)){
                                ?>
                                <div class="top15">

                                  <?php  ?>

                                  <div class="panel panel-default">
                                    <div class="panel-body titulo-mensagem">
                                      <?php Util::imprime($row[titulo]); ?>
                                    </div>
                                    <div class="panel-footer"><?php Util::imprime($row[descricao]); ?></div>
                                  </div>

                                </div>
                                <?php
                              }
                            }
                            ?>


                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.modal -->


                  </div>
                </div>

                <div class="col-xs-12 top40">
                  <div class="form-group input100">
                    <textarea name="mensagem_cartao" cols="30" rows="11" class="form-control fundo-form1 input100" placeholder="MENSAGEM DO CARTÃO"></textarea>
                  </div>
                </div>

                <div class="col-xs-12 top20 form-group ">
                  <select name="identificar_remetente" class="form-control fundo-form1 input-lg input100" placeholder="O REMETENTE DESEJA SE IDENTIFICAR NO CARTÃO?">
                    <option value="">O REMETENTE DESEJA SE IDENTIFICAR NO CARTÃO?</option>
                    <option value="SIM">SIM</option>
                    <option value="NÃO">NÃO</option>
                  </select>
                </div>

                <div class="col-xs-12 text-right ">
                  <div class="top15 bottom25 ">
                    <button type="submit" class="btn btn-formulario" name="btn_cadastrar">
                      CADASTRAR
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- LOCAL DA ENTREGA-->
            <!--  ==============================================================  -->

          </div>

        </div>
      </div>
    </div>


    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <!-- Include Bootstrap Datepicker -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/jquery-ui-1.12.1/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/jquery-ui-1.12.1/jquery-ui.min.css">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/dist/js/formValidation.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/dist/js/framework_bootsrap.js"></script>


  <script>
  $(document).ready(function() {

    $('.FormMensagem')
    .formValidation({
      framework: 'bootstrap',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        data_entrega: {
          validators: {
            notEmpty: {
              message: 'insira data'
            },
            date: {

              format: 'DD-MM-YYYY',
              message: 'data invalída',
              // min and max options can be strings or Date objects
              // min: '18/10/2016',
              // max: '20/12/2030'
            }
          }
        },
        nome_contato: {
          validators: {
            notEmpty: {
              message: 'seu nome por favor'

            }
          }
        },
        celular_contato: {
          validators: {
            notEmpty: {
              message: 'insira seu Telefone'
            },
            phone: {
              country: 'BR',
              message: 'Telefone inválido'
            }
          }
        },
        endereco_entrega: {
          validators: {
            notEmpty: {
              message: 'SEU ENDEREÇO'
            }
          }
        },
        identificar_remetente: {
          validators: {
            notEmpty: {
            }
          }
        },
        numero_entrega: {
          validators: {
            notEmpty: {
            }
          }
        },

        bairro: {
          validators: {
            notEmpty: {

            }
          }
        },
        horario_entrega: {
          validators: {
            notEmpty: {
            }
          }
        },

        cidade: {
          validators: {
            notEmpty: {
            }
          }
        },
        cep_entrega: {
          validators: {
            notEmpty: {
              message: 'INRIRA CEP'

            },
            zipCode: {
              country: 'BR',
              message: 'INRIRA CEP VALIDO NO BRASIL'
            }
          }
        },
        curriculo: {
          validators: {
            notEmpty: {
              message: 'Por favor insira seu currículo'
            },
            file: {
              extension: 'doc,docx,pdf,rtf',
              type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
              maxSize: 5*1024*1024,   // 5 MB
              message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
            }
          }
        },
        mensagem: {
          validators: {
            notEmpty: {

            }
          }
        }

      }
    })
    .find('[name="data_entrega"]')
    .datepicker({
      dateFormat: 'dd-mm-yy',
      minDate: 0,
      showAnim: "fold",
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
      dayNamesMin: ['DOM','SEG','TER','QUA','QUI','SEX','SAB'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],

      onSelect: function(date, inst) {
        /* Revalidate the field when choosing it from the datepicker */
        $('.FormMensagem').formValidation('revalidateField', 'data_entrega');
      }
    });
  });
  </script>
