<?php 
$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/mensagem" );

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">
      <div class="col-xs-12 top35">
       <blockquote>
        <h2>ENVIE SEU PEDIDO<br><span>MEU CARRINHO DE COMPRAS</span></h2>
      </blockquote>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--PROMOCOES DESTAQUES-->
<!--  ==============================================================  -->



<div class="container-fluid bg_fundo_internas">
  <div class="row">
    <div class="container">
      <div class="row">

        
        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4">
          <?php 
          $passo = 2;
          require_once("./includes/lateral_passo_passo.php"); 
          ?>
        </div>
        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->


        <div class="col-xs-4 top120">
          <div class="produtos_destaques">
            <div class="col-xs-12 padding0">
             <blockquote>
               <h2><b>JÁ SOU CADASTRADO</b></h2>
             </blockquote>
           </div>
         </div>

         <!--  ==============================================================  -->
         <!-- SOU CADASTRADO-->
         <!--  ==============================================================  -->
         <?php
          # ==============================================================  #
          # VERIFICO SE E PARA EFETUAR O LOGIN
          # ==============================================================  #
          if(isset($_POST[btn_logar])):
                        
            if($obj_usuario->verifica_usuario($_POST[email], $_POST[senha])):
              
              if (isset($_SESSION[produtos])) {
                Util::script_location(Util::caminho_projeto()."/mensagem");
              }else{
                Util::script_location(Util::caminho_projeto()."/meus-pedidos");
              }
              
            else:
              Util::alert_bootstrap("Usuário ou senha inválido.");
            endif;
                        
          endif;
          ?>

         <div class="col-xs-12  fundo-formulario">
           <form class="form-inline FormLogin pt30" role="form" method="post" enctype="multipart/form-data"> 

            <div class="col-xs-12 top10">
              <div class="form-group  input100">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">    
              </div>
            </div>

            <div class="col-xs-12 top10">
              <div class="form-group input100">
                <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">      
              </div>
            </div>

            
            <div class="col-xs-6 top25">
              <a class="btn1" href="<?php echo Util::caminho_projeto() ?>/recuperar-senha" title="Esqueci minha senha">
                Esqueci minha senha
              </a>
            </div>

            <div class="col-xs-6 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn-formulario" name="btn_logar">
                  ENVIAR
                </button>
              </div>
            </div>

          </form>
        </div>
        <!--  ==============================================================  -->
        <!-- SOU CADASTRADO-->
        <!--  ==============================================================  -->

      </div>

      <div class="col-xs-4  top120">
       <div class="produtos_destaques">
        <div class="col-xs-12 padding0">
         <blockquote>
           <h2><b>NÃO TENHO CADASTRADO</b></h2>
         </blockquote>
       </div>
     </div>
     <!--  ==============================================================  -->
     <!-- SOU CADASTRADO-->
     <!--  ==============================================================  -->
     <div class="col-xs-12  fundo-formulario">
       <?php
        # ==============================================================  #
        # VERIFICO SE E PARA CADASTRAR O USUARIO
        # ==============================================================  #
        if(isset($_POST[btn_cadastrar])):
          
          if($obj_usuario->cadastra_usuario($_POST) != false)
          {
            Util::script_location(Util::caminho_projeto() . "/mensagem");
          }
          else
          {
            Util::alert_bootstrap("Esse email já está cadastrado");
          }
          
        endif;
        ?>
       <form class="form-inline FormCadastro pt30" role="form" method="post" enctype="multipart/form-data"> 

        <div class="col-xs-12 top10">
          <div class="form-group  input100">
            <input type="text" name="nome" class="form-control fundo-form1 input-lg input100" placeholder="NOME">    
          </div>
        </div>

        <div class="col-xs-12 top10">
          <div class="form-group  input100">
            <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">    
          </div>
        </div>

      

        <div class="col-xs-6 top10">
          <div class="form-group  input100">
            <input type="text" name="tel_residencial" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">    
          </div>
        </div>

        <div class="col-xs-6 top10">
          <div class="form-group  input100">
            <input type="text" name="tel_celular" class="form-control fundo-form1 input-lg input100" placeholder="CELULAR">    
          </div>
        </div>

        <div class="col-xs-12 top10">
          <div class="form-group  input100">
            <input type="text" name="endereco" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">    
          </div>
        </div>

        <div class="col-xs-5 top10">
          <div class="form-group  input100">
            <input type="text" name="numero" class="form-control fundo-form1 input-lg input100" placeholder="NÚMERO">    
          </div>
        </div>

        <div class="col-xs-7 top10">
          <div class="form-group  input100">
            <input type="text" name="complemento" class="form-control fundo-form1 input-lg input100" placeholder="COMPLEMENTO">    
          </div>
        </div>

        <div class="col-xs-12 top10">
          <div class="form-group  input100">
            <input type="text" name="bairro" class="form-control fundo-form1 input-lg input100" placeholder="BAIRRO">    
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-7 top10">
          <div class="form-group  input100">
            <input type="text" name="cidade" class="form-control fundo-form1 input-lg input100" placeholder="CIDADE">    
          </div>
        </div>

        <div class="col-xs-5 top10">
          <div class="form-group  input100">
            <input type="text" name="uf" class="form-control fundo-form1 input-lg input100" placeholder="UF">    
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-12 top10">
          <div class="form-group input100">
            <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">      
          </div>
        </div>

        <div class="col-xs-12 top10">
          <div class="form-group input100">
            <input type="password" name="senha2" class="form-control fundo-form1 input100 input-lg" placeholder="CONFIRMAR SENHA">      
          </div>
        </div>

        <div class="col-xs-12 text-right">
          <div class="top15 bottom25">
            <button type="submit" class="btn btn-formulario" name="btn_cadastrar">
              CADASTRAR
            </button>
          </div>
        </div>

      </form>
    </div>
    <!--  ==============================================================  -->
    <!-- SOU CADASTRADO-->
    <!--  ==============================================================  -->

  </div>

  

</div>
</div>
</div>
</div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>





<script>
  $(document).ready(function() {
    $('.FormLogin').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       
      senha: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
     


      }
    });
  });
</script>


<script>
  $(document).ready(function() {
    $('.FormCadastro').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      tel_residencial: {
            validators: {
                  notEmpty: {

                  },
                  phone: {
                      country: 'BR',
                      message: 'Telefone inválido'
                  }
              }
        },
      tel_celular: {
          validators: {
              notEmpty: {

              },
              phone: {
                  country: 'BR',
                  message: 'Telefone inválido'
              }
          }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      numero: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      uf: {
        validators: {
          notEmpty: {

          }
        }
      },

      senha: {
            validators: {
                notEmpty: {

                },
                identical: {
                    field: 'senha2',
                    message: 'As senhas não sào iguais'
                }
            }
        },
        senha2: {
            validators: {
                notEmpty: {

                },
                identical: {
                    field: 'senha',
                    message: 'As senhas não sào iguais'
                }
            }
        }

      }
    });
  });
</script>



