<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 19) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">
      <div class="col-xs-12 top35">
       <blockquote>
        <h2>ENVIE SEU PEDIDO<br><span>MEU CARRINHO DE COMPRAS</span></h2>
      </blockquote>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--PROMOCOES DESTAQUES-->
<!--  ==============================================================  -->



<div class="container-fluid bg_fundo_internas">
  <div class="row">
    <div class="container">
      <div class="row">

        
        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4">
          <?php 
          $passo = 5;
          require_once("./includes/lateral_passo_passo.php"); 
          ?>
        </div>
        <!-- ======================================================================= -->
        <!-- LATERAL ESQUERDA  -->
        <!-- ======================================================================= -->
        






        <div class="col-xs-8 top120 padding0">
          <div class="produtos_destaques">
            <div class="col-xs-12 padding0">
             <blockquote>
               <h2><b>TODOS MEUS PEDIDOS</b></h2>
             </blockquote>
           </div>
         </div>


         <!--  ==============================================================  -->
         <!-- ACCORDING-->
         <!--  ==============================================================  -->
         <div class="col-xs-12 padding0 acording" id="main">

          <div class="panel-group pg20" id="accordion" role="tablist" aria-multiselectable="true">
              <span class="col-xs-2">COD</span >
              <span class="col-xs-3">VALOR DA COMPRA</span>
              <span class="col-xs-5 text-center">STATUS</span>
              <span class="col-xs-2 text-right">DETALHES</span>
              <div class="clearfix"></div>

              
      


              
                  <!--  ==============================================================  -->
                  <!-- DESCRICAO COMPRA 01-->
                  <!--  ==============================================================  -->
                  <?php 
                  $result = $obj_site->select("tb_vendas", "and id_usuario = ".$_SESSION[usuario][idusuario]."");
                  if (mysql_num_rows($result) > 0) {
                    $i = 0;
                    while($row = mysql_fetch_array($result)){
                    ?>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading-<?php echo $i; ?>">
                            <div class="panel-title">
                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $i; ?>"></a>
                              <span class="col-xs-2"><?php Util::imprime($row[0]); ?></span >
                                <span class="col-xs-3 text-center">R$ <?php echo Util::formata_moeda($row[total]); ?></span>
                                <span class="col-xs-5 text-center"><?php Util::imprime($row[status_venda]); ?></span>
                              </div>
                            </div>
                            

                            <!--  ==============================================================  -->
                            <!-- DESCRICAO COMPRA DETALHES 01-->
                            <!--  ==============================================================  -->
                            <div id="collapse-<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $i; ?>">
                              <div class="panel-body">
                                <div class="col-xs-3 text-center">
                                  <b>1</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                  <h3 class="top15">PEDIDO REALIZADO DIA <?php Util::imprime( Util::formata_data($row[data]) , 5); ?></h3>
                                </div>

                                <?php if (!empty($row[data_pagamento]) and $row[data_pagamento] != "0000-00-00" ): ?>
                                <div class="col-xs-3 text-center">
                                  <b>2</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                  <h3 class="top15">PAGAMENTO REALIZADO <br> DIA <?php Util::imprime( Util::formata_data($row[data_pagamento]) , 5); ?></h3>
                                </div>
                                <?php endif; ?>

                                <?php if (!empty($row[data_separacao_entrega]) and $row[data_separacao_entrega] != "0000-00-00" ): ?>
                                <div class="col-xs-3 text-center">
                                  <b>3</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                  <h3 class="top15">PRODUTO SEPARADO PARA ENTREGA <br> DIA <?php Util::imprime( Util::formata_data($row[data_separacao_entrega]) , 5); ?></h3>
                                </div>
                                <?php endif; ?>


                                <?php if (!empty($row[data_entrega_cliente]) and $row[data_entrega_cliente] != "0000-00-00" ): ?>
                                  <div class="col-xs-3 text-center">
                                    <b>4</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                    <h3 class="top15">PRODUTO ENTREGUE DIA <?php Util::imprime( Util::formata_data($row[data_entrega_cliente]) , 5); ?></h3>
                                  </div>
                                <?php endif; ?>

                                

                              </div>
                            </div>
                            <!--  ==============================================================  -->
                            <!-- DESCRICAO COMPRA DETALHES 01-->
                            <!--  ==============================================================  -->


                          </div>  
                    <?php 
                      $i++;
                    }
                  }
                  ?>


                </div>

              </div>
              <!--  ==============================================================  -->
              <!-- ACCORDING-->
              <!--  ==============================================================  -->





              


            </div>

          </div>
        </div>
      </div>
    </div>


    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <script>
    $(document).ready(function() {
      $('.FormCurriculo').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
         nome: {
          validators: {
            notEmpty: {

            }
          }
        },
        email: {
          validators: {
            notEmpty: {

            },
            emailAddress: {
              message: 'Esse endereço de email não é válido'
            }
          }
        },
        telefone: {
          validators: {
            notEmpty: {

            }
          }
        },
        cep: {
          validators: {
            notEmpty: {

            }
          }
        },
        bairro: {
          validators: {
            notEmpty: {

            }
          }
        },
        endereco: {
          validators: {
            notEmpty: {

            }
          }
        },
        dataHora: {
          validators: {
            notEmpty: {

            }
          }
        },
        para: {
          validators: {
            notEmpty: {

            }
          }
        },
        assunto: {
          validators: {
            notEmpty: {

            }
          }
        },
        cidade: {
          validators: {
            notEmpty: {

            }
          }
        },
        cargo: {
          validators: {
            notEmpty: {

            }
          }
        },
        area: {
          validators: {
            notEmpty: {

            }
          }
        },
        estado: {
          validators: {
            notEmpty: {

            }
          }
        },
        mensagem: {
          validators: {
            notEmpty: {

            }
          }
        },
        escolaridade: {
          validators: {
            notEmpty: {

            }
          }
        },
        curriculo: {
          validators: {
            notEmpty: {
              message: 'Por favor insira seu currículo'
            },
            file: {
              extension: 'doc,docx,pdf,rtf',
              type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
    });
  </script>

