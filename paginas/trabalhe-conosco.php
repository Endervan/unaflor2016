<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">

      <div class="col-xs-4 top560">
        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->
       <?php require_once('./includes/contatos.php') ?>
        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->

      </div>

      <div class="col-xs-8 top90">
       <blockquote>
        <h2>ENTRE EM CONTATO<br><span>TRABALHE CONOSCO</span></h2>
      </blockquote>


      <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_POST[nome]))
      {

        if(!empty($_FILES[curriculo][name])):
          $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
        endif;

        $texto_mensagem = "
        Nome: ".$_POST[nome]." <br />
        Telefone: ".$_POST[telefone]." <br />
        Email: ".$_POST[email]." <br />
        Escolaridade: ".$_POST[escolaridade]." <br />
        Cargo: ".$_POST[cargo]." <br />
        Área: ".$_POST[area]." <br />
        Cidade: ".$_POST[cidade]." <br />
        Estado: ".$_POST[estado]." <br />
        Mensagem: <br />
        ".nl2br($_POST[mensagem])."

        <br><br>
        $texto    
        ";


        Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
      }
      ?>



      <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data"> 
        <div class="fundo_formulario_fale">
          <!-- formulario orcamento -->
          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>        
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>        
              </div>
            </div>
          </div>

          <div class="clearfix"></div>   


          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>        
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                <span class="glyphicon glyphicon-book form-control-feedback top10"></span>        
              </div>
            </div>
          </div>

          <div class="clearfix"></div> 

          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                <span class="glyphicon glyphicon-lock form-control-feedback top10"></span>        
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                <span class="glyphicon glyphicon-briefcase form-control-feedback top10"></span>        
              </div>
            </div>
          </div>

          <div class="clearfix"></div> 

          <div class="top20">

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                <span class="glyphicon glyphicon-globe form-control-feedback top10"></span>        
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                <span class="glyphicon glyphicon-globe form-control-feedback top10"></span>        
              </div>
            </div>

          </div>


          <div class="clearfix"></div>

          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                <span class="glyphicon glyphicon-file form-control-feedback top10"></span>        
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-12">        
             <div class="form-group input100 has-feedback">
              <textarea name="mensagem" cols="30" rows="11" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback top15"></span> 
            </div>
          </div>
        </div>

        <!-- formulario orcamento -->
        <div class="col-xs-12 text-right">
          <div class="top15 bottom25">
            <button type="submit" class="btn btn-vermelho" name="btn_contato">
              ENVIAR
            </button>
          </div>
        </div>

      </div>

      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
    </form>

  </div>
</div>
</div>
<!--  ==============================================================  -->
<!--PROMOCOES DESTAQUES-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      telefone: {
         validators: {
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

