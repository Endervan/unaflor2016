<?php
// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if (mysql_num_rows($result) == 0) {
  Util::script_location(Util::caminho_projeto() . "/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


  <script>
    $(function() {
      $('[data-toggle="popover"]').popover()
    })
  </script>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
  .bg-interna {
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- descricao do produto  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top35">

      <div class="col-xs-4">
        <div class="effect2">
          <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 360, 342, array("class" => "input100", "alt" => "$dados_dentro[titulo]")) ?>
        </div>
        <ul class="nav nav-pills nav-stacked produtos_dentro_descricao text-center top70">

          <li role="presentation" class="active">

            <?php if ($config['pausar_vendas'] == "SIM") : ?>

              <p class="btn-produto-esgotado top10">
                PRODUTO ESGOTADO
              </p>
            <?php else : ?>
              <?php
              // VERIFICA SE O PRODUTO TEM PREÇO
              if (!empty($dados_dentro[preco])) {
                // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
              ?>
                <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                  <button type="submit" class="btn btn-comprar top10 input100">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos_dentro01.png" alt="" class="pull-left">
                    COMPRAR AGORA
                  </button>

                  <input type="hidden" name="idproduto" value="<?php echo $dados_dentro[0]; ?>" />
                  <input type="hidden" name="action" value="add" />
                </form>
              <?php } ?>
            <?php endif; ?>
          </li>

          <li role="presentation">

            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos_dentro02.png" class="pull-left">
              HORÁRIO DE ENTREGA
            </a>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">HORÁRIO DE ENTREGA</h2>
                  </div>
                  <div class="modal-body">
                    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
                    <p><?php Util::imprime($row[descricao]); ?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.modal -->


          </li>

          <li role="presentation">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal1">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos_dentro03.png" class="pull-left">
              FORMAS DE PAGAMENTO
            </a>

            <!-- Modal -->
            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">FORMAS DE PAGAMENTO</h2>
                  </div>
                  <div class="modal-body">
                    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
                    <p><?php Util::imprime($row[descricao]); ?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.modal -->

          </li>
        </ul>
      </div>

      <div class="col-xs-8 padding0 descricao_produtos top10">
        <div class="col-xs-6">
          <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
          <p><?php Util::imprime($dados_dentro[codigo]); ?></p>
          <?php if ($config['pausar_vendas'] != "SIM") : ?>
            <h2>R$ <?php echo Util::formata_moeda($dados_dentro[preco]); ?></h2>
          <?php endif; ?>
        </div>

        <div class="col-xs-6">
          <ul class="nav nav-pills nav-stacked produtos_dentro_descricao text-center">
            <li role="presentation" class="active">

              <?php if ($config['pausar_vendas'] == "SIM") : ?>
                <p class="btn-produto-esgotado top10">
                  PRODUTO ESGOTADO
                </p>
              <?php else : ?>

                <?php
                // VERIFICA SE O PRODUTO TEM PREÇO
                if (!empty($dados_dentro[preco])) {
                  // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
                ?>
                  <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                    <button type="submit" class="btn btn-comprar top10 input100">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos_dentro01.png" alt="" class="pull-left">
                      COMPRAR AGORA
                    </button>

                    <input type="hidden" name="idproduto" value="<?php echo $dados_dentro[0]; ?>" />
                    <input type="hidden" name="action" value="add" />
                  </form>
                <?php } ?>

              <?php endif; ?>

            </li>
          </ul>
        </div>

        <div class="col-xs-12  top40">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>

          <div class="top65">
            <h1><b>IMPORTANTE:</b></h1>
          </div>

          <div class="top45">
            <p><?php Util::imprime($dados_dentro[importante]); ?></p>
          </div>

          <div class="col-xs-offset-6 col-xs-6 padding0 top30">
            <ul class="nav nav-pills nav-stacked produtos_dentro_descricao text-center">
              <li role="presentation" class="active">

                <?php if ($config['pausar_vendas'] == "SIM") : ?>
                  <p class="btn-produto-esgotado top10">
                    PRODUTO ESGOTADO
                  </p>
                <?php else : ?>

                  <?php
                  // VERIFICA SE O PRODUTO TEM PREÇO
                  if (!empty($dados_dentro[preco])) {
                    // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
                  ?>
                    <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                      <button type="submit" class="btn btn-comprar top10 input100">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos_dentro01.png" alt="" class="pull-left">
                        COMPRAR AGORA
                      </button>

                      <input type="hidden" name="idproduto" value="<?php echo $dados_dentro[0]; ?>" />
                      <input type="hidden" name="action" value="add" />
                    </form>
                  <?php } ?>

                <?php endif; ?>


              </li>
            </ul>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- descricao do produto  -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_home">
      <div class="col-xs-12 top100">
        <h3>VEJA MAIS PRODUTOS DESSA CATEGORIA</h3>
        <div class="borda_rosa top25"></div>
      </div>



      <?php
      $result = $obj_site->select("tb_produtos", "order by rand() desc limit 4");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
      ?>
          <!--  ==============================================================  -->
          <!-- ITEM 01-->
          <!--  ==============================================================  -->
          <div class="col-xs-3 top5">

            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 259, array("class" => "", "alt" => "$row[titulo]")) ?>
              <h1><?php Util::imprime($row[titulo]); ?></h1>
              <?php if ($config['pausar_vendas'] != "SIM") : ?>
                <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>
              <?php endif; ?>
            </a>

            <?php if ($config['pausar_vendas'] == "SIM") : ?>
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="ESGOTADO" class="btn-produto-esgotado top10">
                PRODUTO ESGOTADO
              </a>
            <?php else : ?>


              <?php
              // VERIFICA SE O PRODUTO TEM PREÇO
              if (!empty($row[preco])) {
                // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
              ?>
                <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto" id="form_produtos">

                  <button type="submit" class="btn btn_produtos_home top10">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                    COMPRAR AGORA
                  </button>



                  <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                  <input type="hidden" name="action" value="add" />
                </form>
              <?php } else { ?>
                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10 ">
                  SAIBA MAIS
                </a>
              <?php } ?>

            <?php endif; ?>

          </div>
      <?php
        }
      }
      ?>



      <div class="clearfix"></div>





    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>