<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- CONHECA UM POUCO MAIS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top95">
      <div class="col-xs-7 col-xs-offset-5">
            
            <div class="linha-titulo">
              <h4>CONHEÇA UM POUCO MAIS A </h4>
              <h4><span>UNAFLOR</span></h4>
            </div>
            
            <div class="detalhe-unaflor top35">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CONHECA UM POUCO MAIS  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- TEMPO E FORMA DE ENTREGA  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top150">
      <div class="col-xs-12">
            
          <div class="linha-titulo">
            <h4>TEMPO E FORMAS DE</h4>
            <h4><span>ENTREGAS</span></h4>
          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
          <p class="top35"><?php Util::imprime($row[descricao]); ?></p>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TEMPO E FORMA DE ENTREGA  -->
  <!-- ======================================================================= -->


  
  <!-- ======================================================================= -->
  <!-- FORMAS DE PAGAMENTO  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top45">
      <div class="col-xs-12">
            
          <div class="linha-titulo">
            <h4>FORMAS DE</h4>
            <h4><span>PAGAMENTO</span></h4>
          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
          <p class="top35"><?php Util::imprime($row[descricao]); ?></p>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- FORMAS DE PAGAMENTO  -->
  <!-- ======================================================================= -->


  

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
