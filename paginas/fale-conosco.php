<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--PROMOCOES  DESTAQUES-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_destaques">

      <div class="col-xs-4 top560">
        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->
        <?php require_once('./includes/contatos.php') ?>
        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->

      </div>

      <div class="col-xs-8 top90">
       <blockquote>
        <h2>ENTRE EM CONTATO<br><span>FALE CONOSCO</span></h2>
      </blockquote>


      <?php
          //  VERIFICO SE E PARA ENVIAR O EMAIL
          if(isset($_POST[nome]))
          {
              $texto_mensagem = "
                                Nome: ".($_POST[nome])." <br />
                                Assunto: ".($_POST[assunto])." <br />
                                Telefone: ".($_POST[telefone])." <br />
                                Email: ".($_POST[email])." <br />
                                Mensagem: <br />
                                ".(nl2br($_POST[mensagem]))."
                                ";
              Util::envia_email($config[email], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
              Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
              
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
          }
          ?>



          <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data"> 
            <div class="fundo_formulario_fale">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top15"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>   


              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback top15"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>  



              <div class="top15">
                <div class="col-xs-12">        
                 <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="30" rows="13" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span> 
                </div>
              </div>
            </div>

            <!-- formulario orcamento -->
            <div class="col-xs-12 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn-formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>

          
          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
        </form>

    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--PROMOCOES DESTAQUES-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

