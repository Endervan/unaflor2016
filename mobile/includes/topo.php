<div class="container">
  <div class="row topo">


    <div class="col-xs-4">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="" class="logo left15">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
          </a>
    </div>


    <div class="col-xs-8">

      <div class="text-right top-btn-topo">
        <h1 class="left50 pull-left right10"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h1>
        <a class=" btn btn-tel-topo" href="tel:+55<?php Util::imprime($config[ddd1].$config[telefone1]); ?>" title="CHAMAR">CHAMAR</a>
      </div>

      <div class="text-right top-btn-topo">
        <h1 class="left50 pull-left right10"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h1>
        <a class=" btn btn-tel-topo" href="tel:+55<?php Util::imprime($config[ddd2].$config[telefone2]); ?>" title="CHAMAR">CHAMAR</a>
      </div>

      <div class="text-right top-btn-topo">
        <h1 class="left50 pull-left right10"><span><?php Util::imprime($config[ddd3]); ?></span> <?php Util::imprime($config[telefone3]); ?></h1>
        <a class=" btn btn-tel-topo" href="tel:+55<?php Util::imprime($config[ddd3].$config[telefone3]); ?>" title="CHAMAR">WHATSAPP</a>
      </div>



    </div>




  </div>
</div>



<!-- ======================================================================= -->
<!-- MENU  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row ">
    <div class="col-xs-12 bg-menu">


        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4 col-xs-offset-3">
          <select name="menu" id="menu-site" class="select-menu top25">
            <option value=""></option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/a-unaflor">CONHEÇA A UNAFLOR</option>
             <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/promocoes">PROMOÇÕES</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</option>

          </select>
        </div>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- PESQUISAS-->
        <!-- ======================================================================= -->
        <div class=" col-xs-2 dropdown text-center">
          <a href="#" class="dropdown-toggle btn btn_pesquisas_topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-search right10"></i>
            <span class="caret"></span></a>

            <ul class="dropdown-menu form_busca_topo pull-right">
              <form class="navbar-form navbar-left" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
                <div class="form-group col-xs-8">
                  <input type="text" class="form-control" placeholder="O que está procurando?" name="busca_produtos">
                </div>
                <button type="submit" class="btn btn-finaliza-orcamento">Buscar</button>
              </form>
            </ul>
          </div>
          <!-- ======================================================================= -->
          <!-- PESQUISAS-->
          <!-- ======================================================================= -->





          <!-- ======================================================================= -->
          <!-- botao carrinho de compra -->
          <!-- ======================================================================= -->
          <div class="col-xs-2 col-xs-offset-1 dropdown text-right pg0">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" class="btn btn_pesquisas_topo input100" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>

            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

             <?php
             if(count($_SESSION[produtos]) == 0)
             {
              ?>
                  <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
              <?php
             }else{
              foreach($_SESSION[produtos] as $key=>$row)
              {
                ?>
                <div class="lista-itens-carrinho">
                  <div class="col-xs-3">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                  </div>
                  <div class="col-xs-9">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                  </div>

                </div>
                <?php
              }
            }


            ?>

            <div class="text-right top10 bottom10 col-xs-12">
              <a class="btn btn-finaliza-orcamento" href="<?php echo Util::caminho_projeto() ?>/mobile/carrinho" title="Finalizar" >
                FINALIZAR
              </a>
            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->





        <div class="menu"></div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MENU  -->
<!-- ======================================================================= -->
