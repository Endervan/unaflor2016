
<div class="clearfix"></div>
<div class="top50"></div>



<div class="container">
	<div class="row">
		<div class="col-xs-6 text-center">
			<img style="width: 50%;" class="pull-left" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/pagamento_pay.jpg" alt="">
			<img style="width: 50%;" class="pull-right" src="<?php echo Util::caminho_projeto() ?>/imgs/bandeiras_pagseguro.jpg" alt="">
		</div>

		<div class="col-xs-6 text-right top15">
			<?php if ($config[facebook] != "") { ?>
			<a href="<?php Util::imprime($config[facebook]); ?>" title="Facebook" target="_blank" class="right20">
				<i class="fa fa-facebook-square  fa-2x" aria-hidden="true"></i>
			</a>
			<?php } ?>

			<?php if ($config[twitter] != "") { ?>
			<a href="<?php Util::imprime($config[twitter]); ?>" title="Twitter" target="_blank">
				<i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
			</a>
			<?php } ?>
		</div>
	</div>
</div>

<div class="container bg-fundo-preto top10">
	<div class="row ">
		<div class=" col-xs-8 text-right">
			<p>TODOS DIREITOS RESERVADOS</p>
		</div>
		<div class="col-xs-4">
			<a href="http://www.homewebbrasil.com.br" class="pull-right top5" target="_blank" >
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-homeweb.png" alt="" height="40">
			</a>

			<?php if ($config[google_plus] != "") { ?>
			<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" class="pull-right right20">
				<p><i class="fa fa-google-plus"></i></p>
			</a>
			<?php } ?>

		</div>
	</div>

</div>


<div class="whatsappFixed">
    <a href="https://api.whatsapp.com/send?phone=55<?php echo Util::trata_numero_whatsapp($config[ddd3].$config[telefone3]); ?>&text=Olá,%20gostaria%20de%20solicitar%20um%20orçamento.">
    <img
                    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/whatsapp.png" 
                    width="50"
                    height="50"
                    alt="<?php echo Util::imprime($row[titulo]) ?>">
    </a> 
</div>
