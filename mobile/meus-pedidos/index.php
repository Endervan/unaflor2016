
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container titulo_int_carrinho">
    <div class="row">
      <div class="col-xs-12 top40">
        <blockquote class="top10">
          <h2>ENVIE SEU PEDIDO<br>
            <span>MEU CARRINHO DE COMPRAS</span>
          </h2>
        </blockquote>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->



  
  <div class="container">
    <div class="row">
      <div class="col-xs-8">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav nav-pills nav-stacked carrinho_formas top35">
          <li role="presentation" class="active"><a href="#"><span>5</span>MEUS PEDIDOS<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
        </ul>
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
      </div>


      <!--  ==============================================================  -->
      <!-- ETAPA DA COMPRA-->
      <!--  ==============================================================  -->
      <div class="col-xs-4 top50">
        <h1>PASSO DE <span>5</span></h1>
      </div>
      <!--  ==============================================================  -->
      <!-- ETAPA DA COMPRA-->
      <!--  ==============================================================  -->
    </div>
  </div>



  <div class="container">
    <div class="row">
      <!--  ==============================================================  -->
      <!-- ACCORDING-->
      <!--  ==============================================================  -->
      <div class="col-xs-12 padding0 acording" id="main">

        <div class="panel-group pg20" id="accordion" role="tablist" aria-multiselectable="true">
        <span class="col-xs-2">COD</span >
            <span class="col-xs-4 padding0 text-right">VALOR DA COMPRA</span>
            <span class="col-xs-3 text-center">STATUS</span>
            <span class="col-xs-3 text-right">DETALHES</span>
            <div class="clearfix"></div>




            <!--  ==============================================================  -->
            <!-- DESCRICAO COMPRA 01-->
            <!--  ==============================================================  -->
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <div class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"></a>
                  <span class="col-xs-3">DAS 0011231</span >
                    <span class="col-xs-3 text-center">R$ 300,00</span>
                    <span class="col-xs-3 text-center">AGUAR .PAGT.</span>
                  </div>
                </div>
                <!--  ==============================================================  -->
                <!-- DESCRICAO COMPRA 01-->
                <!--  ==============================================================  -->

                <!--  ==============================================================  -->
                <!-- DESCRICAO COMPRA DETALHES 01-->
                <!--  ==============================================================  -->
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>1</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PEDIDO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>2</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PAGAMENTO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>3</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTOS SEPARADOS PARA ENTREGA DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>4</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTO ENTREGUE DIA 25/04</h3>
                      </div>
                    </div>
                   
                  </div>
                </div>

              </div>
              <!--  ==============================================================  -->
              <!-- DESCRICAO COMPRA DETALHES 01-->
              <!--  ==============================================================  -->



              
                  <!--  ==============================================================  -->
                  <!-- DESCRICAO COMPRA 02-->
                  <!--  ==============================================================  -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">

                     <div class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"></a>
                      <span class="col-xs-3">DAS 0011231</span >
                        <span class="col-xs-3 text-center">R$ 300,00</span>
                        <span class="col-xs-3 text-center">AGUAR .PAGT.</span>
                      </div>

                    </div>


                    <!--  ==============================================================  -->
                    <!-- DESCRICAO COMPRA DETALHES 02-->
                    <!--  ==============================================================  -->
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                       <div class="panel-body">
                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>1</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PEDIDO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>2</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PAGAMENTO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>3</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTOS SEPARADOS PARA ENTREGA DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>4</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTO ENTREGUE DIA 25/04</h3>
                      </div>
                    </div>
                   
                  </div>
                    </div>
                    <!--  ==============================================================  -->
                    <!-- DESCRICAO COMPRA DETALHES 02-->
                    <!--  ==============================================================  -->

                  </div>
                  <!--  ==============================================================  -->
                  <!-- DESCRICAO COMPRA 02-->
                  <!--  ==============================================================  -->



                  <!--  ==============================================================  -->
                  <!-- DESCRICAO COMPRA 03-->
                  <!--  ==============================================================  -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTree">
                      <div class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTree" aria-expanded="false" aria-controls="collapseTree"></a>
                        <span class="col-xs-3">DAS 0011231</span >
                          <span class="col-xs-3 text-center">R$ 300,00</span>
                          <span class="col-xs-3 text-center">ENTREGUE</span>
                        </div>
                      </div>
                      <!--  ==============================================================  -->
                      <!-- DESCRICAO COMPRA 03-->
                      <!--  ==============================================================  -->

                      <!--  ==============================================================  -->
                      <!-- DESCRICAO COMPRA DETALHES 03-->
                      <!--  ==============================================================  -->
                      <div id="collapseTree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTree">
                        <div class="panel-body">
                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>1</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PEDIDO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>2</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PAGAMENTO REALIZADO DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>3</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTOS SEPARADOS PARA ENTREGA DIA 25/04</h3>
                      </div>
                    </div>

                    <div class="col-xs-12 top40">
                      <div class="col-xs-5">
                        <b>4</b><i class="fa fa-check fa-2x" aria-hidden="true"></i>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="top15">PRODUTO ENTREGUE DIA 25/04</h3>
                      </div>
                    </div>
                   
                  </div>
                      </div>
                      <!--  ==============================================================  -->
                      <!-- DESCRICAO COMPRA DETALHES 03-->
                      <!--  ==============================================================  -->
                    </div>




                  </div>

                </div>
                <!--  ==============================================================  -->
                <!-- ACCORDING-->
                <!--  ==============================================================  -->


              </div>

            </div>
          </div>
        </div>
      </div>




    </div>
  </div>











  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      cep: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      dataHora: {
        validators: {
          notEmpty: {

          }
        }
      },
      para: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

