<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14) ?>
<style>
  .bg-interna {
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container titulo_int">
    <div class="row">
      <div class="col-xs-offset-4 col-xs-8 top40">
        <blockquote class="top10">
          <h2>CONFIRA NOSSOS<br>
            <span>PRODUTOS</span>
          </h2>
        </blockquote>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--barra categorias home-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row barra_categorias top30">
      <div class="col-xs-12 bottom20">
        <select name="select_categorias" class="selectpicker envia_select" data-max-options="1" data-live-search="true" title="TODAS AS NOSSAS CATEGORIAS" data-width="100%">
          <optgroup label="SELECIONE">
            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
            <?php
              }
            }
            ?>
          </optgroup>
        </select>
      </div>



      <form action="" method="post">
        <!--  ==============================================================  -->
        <!-- BARRA CATEGORIA ORDENA POR-->
        <!--  ==============================================================  -->
        <div class="col-xs-4 ordenar_por">
          <select name="filtro_ordem" class="selectpicker" title="ORDENAR POR" data-width="100%" onchange="this.form.submit()">
            <optgroup label="SELECIONE">
              <?php
              //VERIFICO SE FIO SELECIONADO A CATEGORIA
              if ($_GET[get1] == 'ordem' or $_GET[get1] == '') {
                $url_ordem = 'ordem';
              } else {
                $url_ordem = $_GET[get1];
              }
              ?>
              <option value="menor-preco" data-tokens="MENOR PREÇO">MENOR PREÇO</option>
              <option value="maior-preco" data-tokens="MAIOR PREÇO">MAIOR PREÇO</option>
              <option value="mais-vendidos" data-tokens="MAIS VENDIDOS">MAIS VENDIDOS</option>
              <option value="novos-produtos" data-tokens="NOVOS PRODUTOS">NOVOS PRODUTOS</option>

            </optgroup>
          </select>
        </div>
        <!--  ==============================================================  -->
        <!-- BARRA CATEGORIA ORDENA POR-->
        <!--  ==============================================================  -->

        <div class="col-xs-8">

          <!--  ==============================================================  -->
          <!--BARRA PESQUISAS-->
          <!--  ==============================================================  -->
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" value="<?php Util::imprime($_POST[busca_produtos]); ?>">
            <span class="input-group-addon">
              <button type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </span>
          </div>
          <!--  ==============================================================  -->
          <!--BARRA PESQUISAS-->
          <!--  ==============================================================  -->
        </div>

      </form>





      <div class="col-xs-12 top10">
        <div class="barra_rosa"></div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--barra categorias home-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_home">


      <?php
      //  FILTRA PELO TITULO
      if (isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos])) :
        $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
      endif;



      // ORDEM DOS PRODUTOS
      switch ($_POST[filtro_ordem]) {
        case 'menor-preco':
          $complemento_ordem .= "order by preco";
          break;
        case 'maior-preco':
          $complemento_ordem .= "order by preco desc";
          break;
        case 'mais-vendidos':
          $complemento_ordem .= "order by qtd_vendida desc";
          break;
        case 'novos-produtos':
          $complemento_ordem .= "order by idproduto desc";
          break;
      }


      if ($_GET[get1] != 'ordem' and $_GET[get1] != '') {

        $url1 = $_GET[get1];
        //  FILTRA AS CATEGORIAS
        if (isset($url1)) {
          $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);

          $sql = "
                    SELECT 
                    tp.*, tpc.id_categoriaproduto
                  FROM
                    tb_produtos tp, tb_produtos_categorias tpc
                  WHERE 
                    tp.idproduto = tpc.id_produto
                    AND tpc.id_categoriaproduto IN($id_categoria)
                    AND tp.ativo = 'SIM'
                  GROUP BY
                    tp.idproduto 
                    $complemento_ordem
                    ";
        }
      } else {
        $sql = "SELECT * FROM tb_produtos where ativo = 'SIM' $complemento $complemento_ordem";
      }



      $result = $obj_site->executaSQL($sql);
      if (mysql_num_rows($result) == 0) {
        echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
      } else {
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
      ?>
          <div class="col-xs-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 202, 201, array("class" => "", "alt" => "$row[titulo]")) ?>
            </a>
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <h5><?php Util::imprime($row[codigo]); ?></h5>
            <?php if ($config['pausar_vendas'] != "SIM") : ?>
              <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>
            <?php endif; ?>


            <?php if ($config['pausar_vendas'] == "SIM") : ?>
              <a class="btn-produto-esgotado top10" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                PRODUTO ESGOTADO
              </a>
            <?php else : ?>

              <?php
              // VERIFICA SE O PRODUTO TEM PREÇO
              if (!empty($row[preco])) {
                // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
              ?>
                <form action="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho/" method="post" name="form_produto" id="form_produtos">

                  <button type="submit" class="btn btn_produtos_home top10">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                    COMPRAR AGORA
                  </button>



                  <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                  <input type="hidden" name="action" value="add" />
                </form>
              <?php } else { ?>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10">
                  SAIBA MAIS
                </a>
              <?php } ?>

            <?php endif; ?>

          </div>
      <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          } else {
            $i++;
          }
        }
      }
      ?>




    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->





  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>