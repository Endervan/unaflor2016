<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#Carousel').carousel({
        interval: 5000
      })
    });
  </script>



</head>

<body>


  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
                if (mysql_num_rows($result) > 0) {
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) {
                                                                                                            echo "active";
                                                                                                          } ?>"></li>
                <?php
                    $i++;
                  }
                }
                ?>
              </ol>



            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->


        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">


          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
          ?>
              <div class="item <?php if ($i == 0) {
                                  echo "active";
                                } ?>">

                <?php if (!empty($imagem[url])) : ?>
                  <a href="<?php Util::imprime($imagem[url]); ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  </a>
                <?php else : ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <?php endif; ?>

              </div>
          <?php
              $i++;
            }
          }
          ?>

        </div>


      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->



  <div class="pagina">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->

  </div>

  <!--  ==============================================================  -->
  <!--BOTOES FORMAS E ENTREGAS -->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row subir_forma_entregas top60">

      <div class="col-xs-6 bottoes_entrega text-right">
        <a class="btn btn_transparente_entrega" title="FORMAS DE PAGAMENTO" href="javascript:void(0);" data-toggle="modal" data-target="#myModal1">
          FORMAS DE <span class="clearfix">PAGAMENTO</span>
          <span class="posicao_entrega"></span>
        </a>


        <!-- Modal -->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">FORMAS DE PAGAMENTO</h2>
              </div>
              <div class="modal-body">
                <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal -->


      </div>

      <div class="col-xs-6 bottoes_entrega text-right">
        <a class="btn btn_transparente_entrega" href="javascript:void(0);" data-toggle="modal" data-target="#myModal" title="FORMAS DE ENTREGA">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAS DE<span class="clearfix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENTREGA</span>
          <span class="posicao_entrega01"></span>
        </a>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">TEMPO E FORMAS DE ENTREGA</h2>
              </div>
              <div class="modal-body">
                <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal -->



      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--BOTOES FORMAS E ENTREGAS -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--barra categorias home-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row barra_categorias top30">
      <div class="col-xs-12">
        <select name="select_categorias" class="selectpicker envia_select" data-max-options="1" data-live-search="true" title="TODAS AS NOSSAS CATEGORIAS" data-width="100%">
          <optgroup label="SELECIONE">
            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
            ?>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
            <?php
              }
            }
            ?>
          </optgroup>
        </select>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--barra categorias home-->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row produtos_home">


      <?php
      $sql = "
            SELECT 
              tp.*, tpc.id_categoriaproduto
            FROM 
              tb_produtos tp, tb_produtos_categorias tpc
            where 
              tp.idproduto = tpc.id_produto
              AND tp.ativo = 'SIM'
              AND tpc.id_categoriaproduto IN(74, 75)
              order by rand() 
              limit 4
           ";

      $result = $obj_site->executaSQL($sql);
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
      ?>
          <div class="col-xs-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 202, 201, array("class" => "", "alt" => "$row[titulo]")) ?>
            </a>
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <h5><?php Util::imprime($row[codigo]); ?></h5>
            <h2>R$ <?php echo Util::formata_moeda($row[preco]); ?></h2>


            <?php if ($config['pausar_vendas'] == "SIM") : ?>
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="ESGOTADO" class="btn-produto-esgotado top10">
                PRODUTO ESGOTADO
              </a>
            <?php else : ?>

              <?php
              // VERIFICA SE O PRODUTO TEM PREÇO
              if (!empty($row[preco])) {
                // SE NAO TIVER PREÇO NÃO MOSTRA O PREÇO NEM O BOTAO DE COMPRAR
              ?>
                <form action="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho/" method="post" name="form_produto" id="form_produtos">

                  <button type="submit" class="btn btn_produtos_home top10">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_carrinho_home.png" alt="" class="pull-left">
                    COMPRAR AGORA
                  </button>



                  <input type="hidden" name="idproduto" value="<?php echo $row[0]; ?>" />
                  <input type="hidden" name="action" value="add" />
                </form>
              <?php } else { ?>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS" class="btn btn_produtos_home top10">
                  SAIBA MAIS
                </a>
              <?php } ?>

            <?php endif; ?>


          </div>
      <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          } else {
            $i++;
          }
        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--NOSSAS MENSAGENS-->
  <!--  ==============================================================  -->
  <div class="container bottom30">
    <div class="row top40">
      <div class="col-xs-12 relative">
        <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators carroucel_mensagens">
            <?php
            $result = $obj_site->select("tb_promocoes", "order by rand() limit 3");
            if (mysql_num_rows($result) > 0) {
              unset($imagens);
              $i = 0;
              while ($row = mysql_fetch_array($result)) {
                $imagens[] = $row;
            ?>
                <li data-target="#carousel-example-generic1" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) {
                                                                                                        echo "active";
                                                                                                      } ?>"></li>
            <?php
                $i++;
              }
            }
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <?php
            if (count($imagens) > 0) {
              $i = 0;
              foreach ($imagens as $key => $imagem) {
            ?>
                <div class="item <?php if ($i == 0) {
                                    echo "active";
                                  } ?>">

                  <?php if (!empty($imagem[url])) : ?>
                    <a href="<?php Util::imprime($imagem[url]); ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                    </a>
                  <?php else : ?>
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  <?php endif; ?>

                </div>
            <?php
                $i++;
              }
            }
            ?>


          </div>
          <div class="barra_transparente_mensagens effect2"></div>
        </div>

      </div>


      <!--  ==============================================================  -->
      <!-- SEGETÕES DE MENSAGENS-->
      <!--  ==============================================================  -->
      <div class="col-xs-7  sugestoes_home top50">
        <h3>SUGESTÕES DE <br><span>MENSAGENS</span></h3>
      </div>
      <div class="col-xs-12">
        <div class="barra_rosa"></div>
      </div>

      <?php
      $result = $obj_site->select("tb_categorias_mensagens", "order by rand() limit 6");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
      ?>
          <div class="col-xs-6 top15">
            <div class="sugestao pg10">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/sugestoes-mensagens/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <p><?php Util::imprime($row[titulo]); ?></p>
              </a>
            </div>
          </div>
      <?php
        }
      }
      ?>



      <!--  ==============================================================  -->
      <!-- SEGETÕES DE MENSAGENS-->
      <!--  ==============================================================  -->

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--NOSSAS MENSAGENS-->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container bg_conheca_mais">
    <div class="row">
      <div class="col-xs-offset-3 col-xs-9">
        <blockquote class="top10">
          <h2>CONHEÇA UM POUCO MAIS A <br><span>UNAFLOR</span></h2>
        </blockquote>
        <div class="top20">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
          <p><?php Util::imprime($row[descricao], 300000); ?></p>
        </div>
        <a class="btn btn_transparente_saiba pull-right top50" href="<?php echo Util::caminho_projeto() ?>/mobile/a-unaflor" title="A Unaflor">
          SAIBA MAIS
        </a>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->






  <?php require_once('./includes/rodape.php'); ?>




</body>

</html>