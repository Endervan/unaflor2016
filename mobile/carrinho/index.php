
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_carrinho = new Carrinho();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];





# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_POST[action]) and $_POST[action] = 'add'):
  
  $obj_carrinho->add_item($_POST['idproduto']);
  
endif;



# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):
  
  //  ARAMAZENO O LOCAL DE ENTREGA
  $_SESSION[id_bairro_entrega] = $_POST[bairro];
$_SESSION[id_cidade] = $_POST[cidade];
  
  
  if(!isset($_SESSION[usuario])):
    Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
  else:
    Util::script_location(Util::caminho_projeto()."/mobile/mensagem");
  endif;
  
  
endif;



# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if(isset($_POST[btn_atualizar]) or isset($_POST[bairro])):
  
  $obj_carrinho->atualiza_itens($_POST['qtd']);
  $_SESSION[id_cidade] = $_POST[cidade];
  
endif;






# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):
  
  $action = base64_decode($_GET[action]);
  $id = base64_decode($_GET[id]);
  
  //  ESCOLHO A OPCAO
  switch($action):
  
    case 'del':
      $obj_carrinho->del_item($id);
    break;
  
  endswitch;    
  
endif;




?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>

  <script type="text/javascript">
          $(document).ready(function(){
              $('#cidade').change(function(){
                  $('#bairro').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_taxas.php?id='+$('#cidade').val());
              });
          });
  </script>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container titulo_int_carrinho">
    <div class="row">
      <div class="col-xs-12 top40">
        <blockquote class="top10">
          <h2>ENVIE SEU PEDIDO<br>
            <span>MEU CARRINHO DE COMPRAS</span>
          </h2>
        </blockquote>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->



  
  <div class="container">
    <div class="row">
      <div class="col-xs-8">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav nav-pills nav-stacked carrinho_formas top35">
          <li role="presentation" class="active"><a href="javascript:void(0);"><span>1</span>MEU CARRINHO<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
          <?php /*
          <!-- <li role="presentation"><a href="#"><span>2</span>IDENTIFICAÇÃO<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
          <li role="presentation"><a href="#" class="local"><span>3</span>LOCAL DE ENTREGA MENSAGEM<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
          <li role="presentation"><a href="#"><span>4</span>PAGAMENTO<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
          <li role="presentation"><a href="#"><span>4</span>MEUS PEDIDOS<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li> -->
        */ ?>
      </ul>
      <!--  ==============================================================  -->
      <!-- MENU -->
      <!--  ==============================================================  -->
    </div>


    <!--  ==============================================================  -->
    <!-- ETAPA DA COMPRA-->
    <!--  ==============================================================  -->
    <div class="col-xs-4 top50">
      <h1>PASSO DE <span>5</span></h1>
    </div>
    <!--  ==============================================================  -->
    <!-- ETAPA DA COMPRA-->
    <!--  ==============================================================  -->
  </div>
</div>



<form action="<?php echo Util::caminho_projeto(); ?>/mobile/carrinho/" method="post" name="form_produto_final" id="form_produto_final" >
  <div class="container">
    <div class="row">

      <!--  ==============================================================  -->
      <!-- CARRINHO-->
      <!--  ==============================================================  -->
      <div class="col-xs-12 tb-lista-itens top10">
        

        <?php if(count($_SESSION[produtos]) == 0): ?>
             <div class="alert alert-danger">
                <h1 class=''>Nenhum produto foi adicionado ao carrinho.<br></h1>
              </div>           

              <a class="btn btn-rosa-default" href="<?php echo Util::caminho_projeto() ?>/produtos/" class="continuar_comprando">CONTINUAR COMPRANDO</a>

              </div>

        <?php else: ?>


        <table class="table">
          <tbody>


            <?php foreach($_SESSION[produtos] as $key=>$dado):  ?>

            <tr>
              <td>
                <?php $obj_site->redimensiona_imagem("../uploads/$dado[imagem]", 59, 75, array('alt'=>$dado[titulo])); ?>
              </td>
              <td align="left" class="col-xs-4">
                <h1><?php Util::imprime($dado[titulo]) ?></h1>
                <h2><?php Util::imprime($dado[codigo]) ?></h2>
                <h3>R$ <?php echo Util::formata_moeda($dado[preco]) ?></h3>
              </td>
              <td class="text-center">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho_qnt.png" alt="">
                <input type="number" class="input-lista-prod-orcamentos" name="qtd[]" value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                <input name="idproduto[]" type="hidden" value="<?php echo $_SESSION[produtos][idproduto]; ?>"  />
              </td>
              <td class="text-center">
                <a href="?id=<?php echo base64_encode($key) ?>&action=<?php echo base64_encode("del") ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                  <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/excluir.png" alt="">
                </a>
              </td>
            </tr>
            
            <?php $total += $dado[preco] * $dado[qtd]; ?>
            <?php endforeach; ?>





          
          </tbody>
        </table>

        <?php endif; ?>



        <div class="col-xs-6">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-comprar">CONTINUAR COMPRANDO</a>
        </div>


        <div class="col-xs-6">
          <input class="btn-cinza input100" type="submit" name="btn_atualizar" id="btn_atualizar" value="ATUALIZAR CARRINHO"  /> 
        </div>


      </div>
      <!--  ==============================================================  -->
      <!-- CARRINHO-->
      <!--  ==============================================================  -->




      <div class="col-xs-12 top15 carrinho_total">
        <!--  ==============================================================  -->
        <!-- CAL CARRINHO -->
        <!--  ==============================================================  -->
        <div class="media  pull-right">
          <div class="media-left media-middle"> 
            <p></p>
          </div>
          <div class="media-body">
            

            <select name="cidade" id="cidade" class="input100 select-bairros">
              <option value="">Selecione a cidade</option>
                <?php 
               
                $result = $obj_site->select("tb_cidades", "order by uf, titulo");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <option <?php if($row[idcidade] == $_SESSION[id_cidade]){ echo 'selected'; } ?> value="<?php Util::imprime($row[idcidade]); ?>" data-tokens="<?php Util::imprime($row[uf] . ' - ' . $row[titulo]); ?>"><?php Util::imprime($row[uf] . ' - ' . $row[titulo]); ?></option>
                    <?php 
                  }
                }
               
                ?>
          
            </select>

              



            <select name="bairro" id="bairro" class="input100 select-bairros top15" onchange="this.form.submit()">
                <?php 
                if (!empty($_SESSION[id_cidade])) {
                  $result = $obj_site->select("tb_fretes", "and id_cidade = '$_SESSION[id_cidade]' ");
                  if (mysql_num_rows($result) > 0) {
                    while($row = mysql_fetch_array($result)){
                      ?>
                      <option <?php if($row[idfrete] == $_SESSION[id_bairro_entrega]){ echo 'selected'; } ?> value="<?php Util::imprime($row[idfrete]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
                      <?php 
                    }
                  }
                }
                ?>
            </select>


           


          </div>
        </div>
        <div class="col-xs-12 padding0 text-right">
          <h3 class="top15">
            VALOR DA ENTREGA: <b> R$ 
            <?php
              if(isset($_SESSION[id_bairro_entrega])):
                $frete = $obj_carrinho->get_frete($_SESSION[id_bairro_entrega]);
                echo Util::formata_moeda( $frete[valor] );
              endif;
              ?>
              </b>
          </h3>
          <div class="top10 pull-right">
             <p>VALOR TOTAL DO PEDIDO <span>R$ <?php echo Util::formata_moeda($total + $frete[valor]) ?></span>  </p>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="pull-right">

          
          
          <?php if(isset($_SESSION[id_bairro_entrega]) and !empty($_SESSION[id_bairro_entrega]) ): ?>
              <input type="submit" name="btn_enviar" id="btn_enviar" class="btn btn-comprar pull-right top20" value="FINALIZAR PEDIDO"  />      
          <?php endif; ?>



        </div>
        <!--  ==============================================================  -->
        <!-- CAL CARRINHO -->
        <!--  ==============================================================  -->
      </div>

    </div>
  </div>
</form>






<?php require_once('../includes/rodape.php'); ?>

</body>

</html>