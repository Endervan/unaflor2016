<?php 
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto()."/mobile/produtos";
  header("location: $caminho");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if(!isset($_SESSION[produtos])):
  $caminho = Util::caminho_projeto()."/mobile/produtos";
  header("location: $caminho");
endif;





// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 12);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>

  

  
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
<script>
$(function() {
    $("#data_entrega").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
    });

    // desabilitando a digitação do input
    $("#data_entrega").attr( 'readOnly' , 'true' );

});
</script>






  <!-- busca os horarios de entrega -->
  <script type="text/javascript">
  $(function() {


      $('#data_entrega').change(function(){
          $('#horario_entrega').load('<?php echo Util::caminho_projeto(); ?>/includes/horarios_entrega.php?id='+$('#data_entrega').val());
      });


  });
  </script>




</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container titulo_int_carrinho">
    <div class="row">
      <div class="col-xs-12 top40">
        <blockquote class="top10">
          <h2>ENVIE SEU PEDIDO<br>
            <span>MEU CARRINHO DE COMPRAS</span>
          </h2>
        </blockquote>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->



  
  <div class="container">
    <div class="row">
      <div class="col-xs-8">
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
        <ul class="nav nav-pills nav-stacked carrinho_formas top35">
          <li role="presentation" class="active local"><a href="#"><span>3</span>PAGAMENTO<i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
        </ul>
        <!--  ==============================================================  -->
        <!-- MENU -->
        <!--  ==============================================================  -->
      </div>


      <!--  ==============================================================  -->
      <!-- ETAPA DA COMPRA-->
      <!--  ==============================================================  -->
      <div class="col-xs-4 top50">
        <h1>PASSO DE <span>5</span></h1>
      </div>
      <!--  ==============================================================  -->
      <!-- ETAPA DA COMPRA-->
      <!--  ==============================================================  -->
    </div>
  </div>



    <div class="container">
      <div class="row">

        <div class="col-xs-12 produtos_destaques top30">
             <blockquote>
               <h2><b>LOCAL DA ENTREGA</b></h2>
             </blockquote>
         </div>

           <!--  ==============================================================  -->
           <!-- LOCAL DA ENTREGA-->
           <!--  ==============================================================  -->
           <div class="col-xs-12  ">
                  

              <?php
            if(isset($_POST[tipo_pagamento])):
              
              # ==============================================================  #
              # VERIFICO SE FOI ENVIADO A OS DADOS
              # ==============================================================  #
              if(isset($_POST[btn_cadastrar])):
                
                echo $obj_carrinho->finaliza_venda($_POST[tipo_pagamento]);
                
              endif;
            
            else:
            ?>
                          <form name="form_pagamento" id="form_pagamento" class="form_mensagem formulario1" action="" method="post" >
                              <h2>Selecione o tipo de pagamento desejado.</h2>
                              
                              
                              <ul>
                                  
                                  <li class="top20 bottom20">
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="" value="paypal" class="validate[required]" >
                                          PayPal
                                      </p>
                                  </li>
                                  <li class="top20 bottom20">
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="" value="pagseguro" class="validate[required]" >
                                          PagSeguro
                                      </p>
                                  </li>
                                  <li>
                                      <p>
                                          <input type="radio" name="tipo_pagamento" id="tipo_pagamento2" value="deposito" class="validate[required]" >
                                          Depósito bancário
                                      </p>
                                  </li>                    
                            </ul> 
                            
                            <input type="submit" name="btn_cadastrar" id="btn_cadastrar" class="btn btn-formulario top30" value="ENVIAR" />
              </form>
                        
                          
            <?php
            endif;
            ?>


          </div>
          <!--  ==============================================================  -->
          <!-- LOCAL DA ENTREGA-->
          <!--  ==============================================================  -->

    
  </div>
  </div>






  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>

