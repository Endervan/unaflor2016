
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--CONHECA MAIS-->
  <!--  ==============================================================  -->
  <div class="container bg_conheca">
    <div class="row">
      <div class="col-xs-offset-3 col-xs-9 top60">
        <blockquote class="top10">
          <h2>CONHEÇA UM POUCO MAIS A <br><span>UNAFLOR</span></h2>
        </blockquote>
      </div>

      <div class="col-xs-12">
       <div class="top30">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>

    <div class="col-xs-12 top30">
      <blockquote>
        <h2>TEMPO E FORMAS DE<br><span>ENTREGAS</span></h2>
      </blockquote>


      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
      <div class="top30">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    </div>

    <div class="col-xs-12 top30">
      <blockquote>
        <h2>FORMAS DE<br><span>PAGAMENTO</span></h2>
      </blockquote>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
      <div class="top30">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>

</div>
</div>
<!--  ==============================================================  -->
<!--CONHECA MAIS-->
<!--  ==============================================================  -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>